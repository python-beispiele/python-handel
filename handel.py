import random
from datetime import datetime

class Spieler:
    
    def __init__(self, name, saldo) -> None:
        self.name = name
        self.saldo = saldo
        self.tasche = {}

    def __str__(self) -> str:
        return f"{self.name}, {self.saldo} Euro, Waren: {self.tasche}"
    
    def kaufen(self, ware, anzahl):
        preis = marktplatz.get(ware)[0]
        if preis*anzahl <= self.saldo:
            print(f"Du kaufst {anzahl} {ware} für {preis*anzahl} Euro")
        else:
            print("Du hast nicht genug Geld.")
            return

        if ware in self.tasche:
            v_anzahl, v_preis = self.tasche[ware]
            neue_anzahl = v_anzahl + anzahl
            durch_preis = round((v_anzahl*v_preis + anzahl*preis)/(v_anzahl + anzahl),1)
            self.tasche[ware] = (neue_anzahl, durch_preis)
        else:
            self.tasche[ware] = (anzahl, preis)
        self.saldo -= preis*anzahl

    def verkaufen(self, ware, anzahl):
        preis = marktplatz.get(ware)[0]
        if self.tasche[ware] and anzahl <= self.tasche[ware][0]:
            print(f"Du verkaufst {anzahl} {ware} für {preis*anzahl} Euro")
        else:
            print(f"Du hast nicht genug {ware}.")
            return

        v_anzahl, v_preis = self.tasche[ware]
        if v_anzahl-anzahl == 0:
            self.tasche.pop(ware)
        else:
            self.tasche[ware] = (v_anzahl-anzahl, v_preis)
        
        self.saldo += preis*anzahl 

def oeffneMarktplatz():
    marktplatz.clear()
    for ware in waren:
        preis = random.randint(ware[1],ware[2])
        marktplatz[ware[0]] = [preis]
    for i, (w, p) in enumerate(marktplatz.items()):
        print(f"{i+1} {w}: {p[0]} Euro")

def neuesZiel():
    ziel = -1
    for i in range(len(staedte)):
        print (i + 1, " ", staedte[i])
    while not (ziel >= 0 and ziel <= len(staedte)):
        ziel = int(input("Wo soll es hin gehen? "))
    return ziel

def save_highscore():
    with open("highscore.txt", "a") as file:
        file.write(f"{spieler.name};{spieler.saldo};{datetime.now().strftime("%Y-%m-%d %H-%M")}\n")

waren = [("Honig",5,15), ("Salz",8,20), ("Getreide",10,25), ("Tran",15,30), ("Stoffe",20,40), ("Pelze",40,80)]
staedte = ["Lübeck","Bergen","Göteborg","Ystad","Riga"]
spieler = Spieler("Spieler 1", 100)
marktplatz = {}

stadt = 1
tag = 0
anz_tage = 5 # int(input("Wie viele Tage willst du spielen? "))

while tag < anz_tage:
    tag += 1

    print("===========================================================")
    print(f"{str(tag) + '.' if tag < anz_tage else 'Letzter'} Tag - {staedte[stadt - 1]}")
    print("----------------------")
    oeffneMarktplatz()
    print("----------------------")
    aktion = ""

    while not aktion == "r": # r = Reisen, k = Kaufen, v = Verkaufen
        print(spieler)
        aktion = input("Dein nächster Zug? r = Reisen, k = Kaufen, v = Verkaufen : ")
        if aktion == "k":
            kauf = input("Was möchtest du kaufen? ")
            ware, anzahl = kauf.split()
            spieler.kaufen(ware, int(anzahl))
        elif aktion == "v":
            verkauf = input("Was möchtest du verkaufen? ")
            ware, anzahl = verkauf.split()
            spieler.verkaufen(ware, int(anzahl))
        elif aktion == "r":
            if tag < anz_tage:
                stadt = neuesZiel()
        else:
            print("Ungültige aktion! Nochmal ...")
    print("===========================================================\n")

print(f"Du hast am Ende {spieler.saldo} Euro - ENDE")
save_highscore()